## Waykichain Weekly Status Report

* 技术部周报 
  * 个人周报
    * 发送邮箱： ```rdg @ waykichainhk dot com```
    * 每周五下班前完成发送
    * 发送核心内容
      * 本周完成工作（需要量化）
      * 下周工作规划（SMART）
      * 其它
 * 技术部周报汇总
   * 直接在本项目内编写，一月一文件方式保存汇总周报
   * 轮班人员名册和顺序
   
```javascript
   - CXH 陈信华(chenxinhua2018)
   - CJQ 陈俊强(cjq)
   - JWQ 蒋文强(jiangwenqiang1992)
   - PSS 潘杉杉(panshan)
   - WWH 王炜豪(wangweihao)
   - XSH 辛舒浩(xinshuhao)
   - Xj  肖江(xiaojiang)

```
```javascript
   - TJ 陶静(taojingtaojing)
   - CLY 陈艳菱(pudding9007)
   - HSY 韩松佑(hansy1121@icloud.com)
```


   * 轮班汇总开始日期：```2018-09-08```
     * 轮班汇总完成时间：```每周六中午12点钟之前```
     * 汇总范围：
       * 技术部集体员工本周项目进展和下周规划
       * 注意信息安全，内部信息不对外披露
       * 最后发布由CTO审阅后定稿
