﻿# 【3月28日周报】
## 本周工作总结：
### 公链建设
1. WASM VM内核版本同步到社区最新上线版本(100%)
2. 完成一对多转账接口(100%)
3. 完成跨链币对的治理(100%)
4. 完成限制得票数达到21,000时，候选人才能成为矿工(100%)
5. 完善DEX交易币对的去中心化权限治理(100%) 
6. 交易序列化API的开发(30%)
7. 实现交易所代交DEX挂单的交易手续费(100%)
8. 测试公链V3.0兼容V2.0版本测试链的数据(30%)
9. 公链V3.0 UTXO测试(70%)
10. 公链V3.0 参数治理自动化测试用例添加(20%)


### 应用建设
1. CDP V1.0.4版本测试及上线，增加CDP详情页操作详情可查看、抵押率可精准输入(100%)
2. ETH跨链流程与节点功能测试(80%)
3. DEX V1.1版本开发，优化挂单体验和行情数据展示(100%)
4. 鹰眼项目V1.0 版本开发(100%)
5. 官网的日常维护


## 下周工作计划：
### 公链建设：
1. WASM 集成系统压力测试
2. 交易序列化API的开发
3. 测试公链V3.0兼容V2.0版本测试链的数据
4. 公链V3.0 UTXO测试
5. 公链V3.0 测试用例设计完善与测试执行推进


### 应用建设：
1. DEX V1.1测试及上线
2. 鹰眼项目V1.0版本测试
3. 维基时代部分机型兼容问题调试
4. 修改BaaS以支持公链V3.0
5. 维基链与以太坊跨链生成mETH和销毁mETH


# 【3月20日周报】
## 本周工作总结：
### 公链建设
1. 喂价中位数计算方法增加了计算窗口大小变更可治理功能(100%)
1. 完善了喂价币币对的去中心化权限治理(100%)
1. 优化了CDP币对列表的去中心化治理(100%)
1. 完善了CDP抵押币权限的去中心化治理(100%)
1. 修复了转账提议的错误(100%)
1. 修复了UTXO内条件判断错误未处理的问题(100%)
1. 实现了跨链的提议+附议交易(100%)
1. 完成了抵押物拍卖行功能的WASM智能合约(100%)
1. 完成了基于WASM智能合约通用提议复议的框架实现(100%)
1. 公链v3.0测试：开展了去中心化参数治理和UTXO的全面测试与相关BUG修复
1. 公链v3.0测试：增加了整体上层业务相关测试用例补充
1. 公链v3.0测试：优化了公链自动化测试框架与测试报告显示

### 应用建设
1. 新增BaaS服务API接口：支持了读取类操作的HTTP GET方法支持 (100%)
1. ETH跨链流程在链外模块功能的设计细化(70%)
1. 维基时代APP扫码功能文档整理(60%)
1. Kotlin钱包库添加NickID注册交易(100%)
1. 扫链服务升级，支持一对多等多种交易解析(70%）
1. 鹰眼项目统计数据库设计(80%)
1. 完成鹰眼项目总览和相关页面的开发(100%)
1. 完成WGRT的锁仓合约，待测试(80%)
1. 跨链以太坊合约设计和开发(20%)
1. CDP移动端V1.0.3测试及上线(100%)
1. CDP移动端V1.0.4开发(100%)

## 下周工作计划：
### 公链建设：
1. 完善DEX交易币对的去中心化权限治理
2. 完善账户相关权限的去中心化权限治理
3. 转账交易增加收据信息
4. WASM VM内核版本同步到社区最新上线版本
5. 公链v3.0测试用例设计完善与测试执行推进

### 应用建设：
1. 完成跨链以太坊合约开发
2. 完成扫链服务升级
3. DEX移动端v1.1开发与细节优化
4. 鹰眼项目概览开发，包括持币分布、交易详情等
5. CDP移动端v1.0.4测试及上线



# 【3月13日周报】
## 本周工作总结：
### 公链建设
- 实现WASM VM合约内存访问错误捕获功能 100%
- 拍卖行合约的编写 70%
- 改进投票排序列表，以定长LEB128编码存储票数 100%
- 改进DEX挂单相关的交易 100%
- 增加版本3的软分叉检查，使得部分新加的交易必须在版本3后才能使用 100%
- 重新审视提议附议、PBFT等功能的逻辑实现，并进行代码优化 100%
- 实现UTXO的RPC开发 40%
- 公链V3.0测试需求梳理：去中心化链上治理(80%)，全资产去中心化交易和多交易所支持(90%)，UTXO(60%)
- 公链V3.0参数治理测试用例编写 80%
- 公链V3.0参数治理接口正常流程测试 30%

### 应用建设
- CDP项目前后端代码优化，接口增加CDP清算后可返回的WICC数量等功能 50%
- 开发WICC/WGRT统一价格查询接口 100%
- 冷钱包离线签名项目开发完成 100%
- 鹰眼项目总揽WICC持币分布页面编写 50%
- 鹰眼项目后端功能分析，数据库设计 30%
- 开发WUSD汇率转换接口、开发WICC的24小时行情交易接口 100%
- 喂价机制新增调整参数设置，喂价币种调整为可配置及价格变动较小时喂价机制调整 70%
- 设计并执行WUSD兑换项目测试用例 40%


## 下周工作计划：
### 公链建设：
- 完成拍卖行合约及相应api实现
- WASM VM更新到最新版本
- 改进中位数计算方法以支持计算窗口大小的变更
- 改进投票统计，限制得票数必须达到21000才能成为生产者
- 完成UTXO相关签名接口的开发
- 继续进行公链自动化测试
- 公链V3.0参数治理继续测试及相关bug跟进
- 完成公链V3.0测试需求梳理
- 公链V3.0用例设计与测试执行

### 应用建设：
- 鹰眼项目总揽WICC持币分布页面编写及后端的开发
- 完成DEX、CDP前后端代码优化，解决DEX加载慢的问题
- 完成喂价项目的优化
- 执行WUSD兑换项目测试用例